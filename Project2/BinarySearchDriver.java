import java.util.Arrays;

public class BinarySearchDriver {
	public static void main(String[] args) {
		int arr[] = {1,5,8,14,30,40,100};
		System.out.println("Array for binary search: " + Arrays.toString(arr));
		System.out.println("Finding aKey = 40");
		int x = BinarySearch.binarySearch(arr, 0, arr.length -1, 40); 		//aKey is 40
		if(x == -1)
			System.out.println("The value is not present");
		else
				System.out.println("The aKey is located at index " + x); 	//print the index where aKey is located
		
	}
	
}
