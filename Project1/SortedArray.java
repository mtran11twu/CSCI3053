import java.util.ArrayList;
import java.util.Collections;

public class SortedArray {
	public void fetch(Listing[] Listings, String id) {
		// if student's ID is in the array, it will fetch student data and turn the boolean true
		boolean b = false;
		for (int i = 0; i < Listings.length; i++) {
			if (Listings[i] != null) {
				if (Listings[i].getID().equals(id)) {
					Listing studentList = Listings[i];
					System.out.println(studentList);
					b = true;
				}
			}
		}
		// if student ID is not found on the array, it can not fetch student data (boolean stays false)
		if (!b) {
			System.out.println("Couldn't find the student. Please try it again.");
		}

	}
	public Listing[] delete(Listing[] Listings, String id) {
		// if student's ID is in the array, it will delete student data and turn the boolean true
		boolean isPresent = false;
		for (int i = 0; i < Listings.length; i++) {
			if (Listings[i] != null) {
				if (Listings[i].getID().equals(id)) {
					Listings[i] = null;
					isPresent = true;
					System.out.println("Student information has been deleted in the system.");
				}
			}
		}
		// if student's ID is not found on the array, it will give an error message (boolean stays false)
		if (!isPresent) {
			System.out.println("Couldn't find the student. Please try it again.");
		}

		return Listings;
	}

	public Listing[] update(Listing[] Listings, String id, String NewInfo, String InfoUpdate) {
		boolean isPresent = false;
		// if student's ID is in the array, it will update the student data and turn the boolean true
		for (int i = 0; i < Listings.length; i++) {
			if (Listings[i] != null) {
				if (Listings[i].getID().equals(id)) {
					Listing list = Listings[i];
					if (InfoUpdate.equalsIgnoreCase("name")) {
						list.setName(NewInfo);
						isPresent = true;
					} else if (InfoUpdate.equalsIgnoreCase("GPA")) {
						list.setGPA(NewInfo);
						isPresent = true;

					}
					Listings[i] = list;
					break;
				}
			}
		}
		// if the student's ID is not found in the array, it will give an error message (boolean stays false)
		if (!isPresent) {
			System.out.println("Couldn't find the student. Please try it again.");
		}
		return Listings;
	}

	public void fetch(Listing[] Listings) {
		// List of object of the Listing class
		ArrayList<Listing> arraylist = new ArrayList<>();
		for (int i = 0; i < Listings.length; i++) {
			if (Listings[i] != null) {
				arraylist.add(Listings[i]);
			}
		}
		// Calling the Collections.sort to sort the list that implements comparable interface
		Collections.sort(arraylist);
		for (int i = 0; i < arraylist.size(); i++) {
			System.out.println(arraylist.get(i));
		}
	}
}
