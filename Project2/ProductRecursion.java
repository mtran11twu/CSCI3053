
public class ProductRecursion {
	public static int productNumbers(int a, int b) {
		if (b == 0) {
			return 0; // Base case
		}

		return a + productNumbers(a, b - 1); // General solution
	}
}
