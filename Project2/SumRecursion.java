
public class SumRecursion {
	public static int sumNumbers(int a, int b) {
		if (a == b) {
			return a; // Base case
		}

		return a + sumNumbers(a + 1, b); // General solution
	}
}
