
public class ArrayRecursionDriver {
	public static void main(String[] args) {
		char c[] = { 'd', 'a', 't', 'a' };
		ArrayRecursion.printArray(c, 0, c.length - 1); // prints the array starting at index 0
	}
}