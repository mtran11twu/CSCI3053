
public class ArrayRecursion {
	public static void printArray(char c[], int start, int end) {
		if (start > end) {
			return; // Base case
		}

		System.out.print(c[start] + " ");
		printArray(c, start + 1, end); // General solution
	}
}
