
public class BinarySearch {
	public static int binarySearch(int arr[], int low, int high, int aKey) {
		if (high >= low) {
			int mid = low + (high - low) / 2;
			if (arr[mid] == aKey) {
				return mid; // Base case
			}

			if (arr[mid] > aKey) // General solution
				return binarySearch(arr, low, mid - 1, aKey); // Recursively searches lower half of array
			return binarySearch(arr, mid + 1, high, aKey); // Recursively searches upper half of array
		}
		return -1;
	}
}
