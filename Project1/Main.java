import java.util.*;

public class Main {

	//Scanner for user input
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		SortedArray sortedArray = new SortedArray();
		
		// Asks for user input for maximum data set size
		System.out.println("Please enter maximum size of students: ");
		int maximum = scan.nextInt();
		
		// Asks for initial number for the data set
		System.out.println("Please enter initial number of students: ");
		int initial = scan.nextInt();

		// Creates new Listing with maximum data size set
		Listing[] Listings = new Listing[maximum];

		// For loop to enter in student's information equal to the initial data set size
		for (int z = 0; z < initial; z++) {
			System.out.println("Please add student #" + (z + 1) + "'s information.");
			Listing list = AddToList();
			Listings[z] = list;
		}

		// Menu options
		// Infinite loop until the user inputs option 6
		while (true) {
			System.out.println("Please enter the number from the selection: ");
			System.out.println("Enter: 1 to insert a new student's information, ");
			System.out.println("2 to fetch and output a student's information, ");
			System.out.println("3 to delete a student's information, ");
			System.out.println("4 to update a student's information, ");
			System.out.println("5 to output all the student information in sorted order, ");
			System.out.println("6 to exit the program. ");

			// Asks for menu number input
			int numInput = scan.nextInt();

			// The user number input will trigger specific cases
			switch (numInput) {

			// Case 1 will ask to insert new student's information
			case 1:
				Listing list = AddToList();
				Listings[initial++] = list;
				break;

			// Case 2 will fetch a student's information
			case 2:
				System.out.println("Which student would you like to find? Please enter student's ID: ");
				String id = scan.next();
				sortedArray.fetch(Listings, id);
				break;

			// Case 3 will delete a student's information
			case 3:
				System.out.println("Which student would you like to delete? Please enter student's ID: ");
				String id1 = scan.next();
				Listings = sortedArray.delete(Listings, id1);
				break;

			// Case 4 will update student's information by asking for a new name or GPA. ID will remain unchanged
			case 4:
				String InfoUpdate = null;
				String NewInfo = null;

				System.out.println("Which student would you like to update? Please enter student's ID: ");
				String id2 = scan.next();
				System.out.println("Enter 1 to change the name of the student" + "\n" + "Enter 2 to change the GPA"
						+ "\n" + "Please know student's ID cannot be changed ");

				int update = scan.nextInt();

				// Nested switch case to be able to enter in a new name or GPA
				// To change both the name and GPA, the user would need to input the same case option again and same ID# then select the other option to update
				switch (update) {
				case 1:
					System.out.println("Enter name : ");
					NewInfo = scan.next();
					InfoUpdate = "name";
					break;
				case 2:
					System.out.println("Enter GPA : ");
					NewInfo = scan.next();
					InfoUpdate = "GPA";
					break;

				default:
					break;
				}

				Listings = sortedArray.update(Listings, id2, NewInfo, InfoUpdate);
				break;

			// Case 5 will output the data set in sorted order
			case 5:
				sortedArray.fetch(Listings);
				break;

			//Case 6 will exit the menu program
			case 6:
				System.out.println("Thank you. See you next time!");
				System.exit(0);
				break;
			}

		}

	}

	// Inserting student's information to the array
	private static Listing AddToList() {
		System.out.println("Please enter student's name : ");
		String name = scan.next();
		System.out.println("Please enter student's ID number : ");
		String id = scan.next();

		System.out.println("Please enter student's GPA : ");
		String gpa = scan.next();
		Listing list = new Listing(name, id, gpa);
		return list;
	}

}
