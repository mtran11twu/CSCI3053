public class Listing implements Comparable<Listing> {

	private String name;
	private String id;
	private String gpa;

	public Listing(String name, String id, String gpa) {
		this.name = name;
		this.id = id;
		this.gpa = gpa;
	}

	// Getter and methods for name, id and GPA
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public String getGPA() {
		return gpa;
	}

	public void setGPA(String gpa) {
		this.gpa = gpa;
	}

	// toString method to print the student's information
	public String toString() {
		return "Student name: " + name + "\n" + "Student ID: " + id + "\n" + "Student GPA: " + gpa + "\n";
	}

	// Compare method to compare ID values in sortedArray class
	public int compareTo(Listing list) {
		return this.getID().compareTo(list.getID());
	}

}
