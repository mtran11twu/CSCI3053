
public class MainFibonacci {
	public static void main(String[] args) {
		for (int i = 1; i <= 45; i++) { // Limits the output to the first 45 items for the Fibonacci sequence
			System.out.println("Fibonacci" + i + " " + fiboSequence(i));
		}
	}

	// Non dynamic recursive method
	public static int fiboSequence(int n) {
		if (n == 1 || n == 2) { // First 2 terms are 1 in the Fibonacci sequence
			return 1;
		} else {
			int rp1 = fiboSequence(n - 1); // Reduced problem 1
			int rp2 = fiboSequence(n - 2); // Reduced problem 2
			int gs = rp1 + rp2; // General solution
			return gs;
		}
	}
}

//This solves programming exercise question 20